﻿using System;
using System.Collections.Generic;
using GameMechanics.Characters;
using GameMechanics.Characters.Heroes;
using Commands;
using Commands.Commands;

namespace GameMechanics.StateMachines.Ninja
{
    public class IdleState : IState
    {
        INinja ninja;

        public IdleState(INinja ninja)
        {
            this.ninja = ninja;
        }

        public void OnEnter()
        {
            ninja.SetAnimation(CharacterAnimationsList.Idle);
        }

        public void Update(int gameTime, ICommand command)
        {            
            switch(command.Type)
            {
                case CommandType.AttackCommand:
                    ninja.ActualState = ninja.AttackState;
                    command.Execute(ninja);
                    break;
                case CommandType.JumpCommand:
                    ninja.ActualState = ninja.JumpState;
                    command.Execute(ninja);
                    break;
                case CommandType.LeftCommand:
                case CommandType.RightCommand:
                    ninja.ActualState = ninja.RunState;
                    command.Execute(ninja);
                    break;
                    
            }
           
        }
    }
}

