﻿
using GameMechanics.Characters;
using GameMechanics.Characters.Heroes;
using Commands.Commands;
using Commands.Actors;

using Services;
using Services.Audio;

namespace GameMechanics.StateMachines.Ninja
{
    public class AttackState : IState
    {
        INinja ninja;
        public AttackState(INinja ninja)
        {
            this.ninja = ninja;
        }

        public void OnEnter()
        {            
            ninja.SetAnimation(CharacterAnimationsList.Attack);
        }

        public void Update(int gameTime, ICommand command)
        {

            switch (command.Type)
            {
                case Commands.CommandType.AttackCommand:
                    if (ninja.IsCurrentAnimationDone())
                    {
                        command.Execute(ninja);
                    }
                    break;
                case Commands.CommandType.NothingCommand:
                    if (ninja.IsCurrentAnimationDone())
                    {
                        ninja.ActualState = ninja.IdleState;
                        command.Execute(ninja);
                    }
                    break;
                case Commands.CommandType.RightCommand:
                case Commands.CommandType.LeftCommand:
                    if (ninja.IsCurrentAnimationDone())
                    {
                        ninja.ActualState = ninja.RunState;
                        command.Execute(ninja);
                    }
                    break;
                default:
                    break;
            }    
        }
    }
}
