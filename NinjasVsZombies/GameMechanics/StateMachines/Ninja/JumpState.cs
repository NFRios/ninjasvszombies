﻿using GameMechanics.Characters;
using GameMechanics.Characters.Heroes;
using Microsoft.Xna.Framework;
using Commands.Commands;

namespace GameMechanics.StateMachines.Ninja
{
    public class JumpState : IState
    {
        public INinja ninja;
        float floorYPosition = GameConstants.FloorYPosition;
        public Vector2 r;

      

        public JumpState(INinja ninja)
        {
            this.ninja = ninja;
        }

        public void OnEnter()
        {
            ninja.SetAnimation(Characters.CharacterAnimationsList.Jump);
           
            r = new Vector2(ninja.XMovementMagnitude, -20);
        }

        public virtual void Update(int gameTime, ICommand command)
        {           

            switch(command.Type)
            {
                case Commands.CommandType.AttackCommand:
                    ninja.ActualState = ninja.AttackWhileJumpState;
                    ((AttackWhileJumpState)(ninja.ActualState)).OnEnter(r);
                    command.Execute(ninja);
                    break;
                case Commands.CommandType.NothingCommand:
                    command.Execute(ninja);
                    break;
                default:
                    break;
            }

            r += GameConstants.GravityVector;
            ninja.Position += r;          

            if (ninja.Position.Y > floorYPosition)
            {
                ninja.Position = new Vector2(ninja.Position.X, floorYPosition);

                ninja.ActualState = ninja.IdleState;

            }
        }
    }
}
