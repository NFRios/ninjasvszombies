﻿using GameMechanics.Characters;
using Commands.Commands;

using Microsoft.Xna.Framework;

namespace GameMechanics.StateMachines.Ninja
{
    public class AttackWhileJumpState:JumpState
    {

        public AttackWhileJumpState(INinja ninja):base(ninja)
        {
           
        }

        public void OnEnter(Vector2 momentum)
        {
            r = momentum;
            ninja.SetAnimation(Characters.CharacterAnimationsList.AttackWhileJump);
        }

        public override void Update(int gameTime, ICommand command)
        {

            if (ninja.IsCurrentAnimationDone())
            {
                command.Execute(ninja);
            }

            command = new JumpCommand();

            base.Update(gameTime, command);
        }
    }
}
