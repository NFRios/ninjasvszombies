﻿
using GameMechanics.Characters;
using GameMechanics.Characters.Heroes;
using Commands.Commands;

namespace GameMechanics.StateMachines.Ninja
{
    public class MoveState : IState
    {
        INinja ninja;
        public MoveState(INinja ninja)
        {
            this.ninja = ninja;
        }
        public void OnEnter()
        {
            ninja.SetAnimation(CharacterAnimationsList.Run); 
        }

        public void Update(int gameTime, ICommand command)
        {    
            
            switch(command.Type)
            {
                case Commands.CommandType.JumpCommand:
                    ninja.ActualState = ninja.JumpState;
                    command.Execute(ninja);
                    break;
                case Commands.CommandType.AttackCommand:
                    ninja.ActualState = ninja.AttackState;
                    command.Execute(ninja);
                    break;
                case Commands.CommandType.NothingCommand:
                    ninja.ActualState = ninja.IdleState;
                    command.Execute(ninja);
                    break;
                case Commands.CommandType.LeftCommand:
                case Commands.CommandType.RightCommand:
                    command.Execute(ninja);
                    break;
            }
           
        }
    }
}
