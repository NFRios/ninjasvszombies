﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Commands.Commands;

namespace GameMechanics.StateMachines
{
    public interface IState
    {
        void OnEnter();
        void Update(int gameTime, ICommand command);
    }
}
