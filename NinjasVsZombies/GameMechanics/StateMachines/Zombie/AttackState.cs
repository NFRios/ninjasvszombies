﻿using System;
using Commands.Commands;
using GameMechanics.Characters;

namespace GameMechanics.StateMachines.Zombie
{
    public class AttackState:IState
    {
        IZombie zombie;
        public AttackState(IZombie zombie)
        {
            this.zombie = zombie;
        }

        public void OnEnter()
        {
            zombie.SetAnimation(CharacterAnimationsList.Attack);
        }

        public void Update(int gameTime, ICommand command)
        {
            switch(command.Type)
            {

            }
        }
    }
}
