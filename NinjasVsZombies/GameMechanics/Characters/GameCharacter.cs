﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Commands.Actors;
using Textures;
using Textures.Animations;
using Handlers;

using GameMechanics.StateMachines;

namespace GameMechanics.Characters
{
    public abstract class GameCharacter : IActor, IPositionable
    {
        public bool IsOnGround
        {
            get;
            set;
        }

        /// <summary>
        /// The number of pixels moved per command, by default is 1
        /// </summary>
        public int MovementFactor = 1;     

        /// <summary>
        /// The animations of this character
        /// </summary>
        protected CharacterAnimations Animations;

        /// <summary>
        /// The textures of this character
        /// </summary>
        protected TexturePack CharTextures;

        /// <summary>
        /// The size scale of this character
        /// </summary>
        public float Scale;

        /// <summary>
        /// The max hp this character has
        /// </summary>
        public int HealthPoints;        

        /// <summary>
        /// the damage this char's got
        /// </summary>
        public int Damage;

        public int XMovementMagnitude { get; set; }


        public bool DrawOnScreen = true;

        /// <summary>
        /// The actual state the character is in
        /// </summary>
        public IState ActualState
        {
            get
            {
                return actualState;
            }
            set
            {
                if (value != actualState)
                {
                    actualState = value;
                    actualState.OnEnter();
                }
            }
        }

        private IState actualState;

        public Vector2 Position
        {
            get { return this.Animations.Position; }
            set { this.Animations.Position = value; }

        }

        protected bool isFacingLeft;

        public void MoveTo(int x, int y)
        {
            Position = new Vector2(x, y);
        }

        public void MoveUp()
        {
            //Position.Y -= MovementFactor;
        }

        public void MoveDown()
        {
            //Position.Y += MovementFactor;
        }

        public virtual void MoveLeft()
        {
            //Position.X -= MovementFactor;
            isFacingLeft = true;
            Vector2 pos = new Vector2(Position.X - MovementFactor, Position.Y);
            MoveTo((int)pos.X, (int)pos.Y);
            XMovementMagnitude = -MovementFactor;
        }

        public virtual void MoveRight()
        {
            isFacingLeft = false;
            Vector2 pos = new Vector2(Position.X + MovementFactor, Position.Y);
            SetPosition(pos);
            XMovementMagnitude = MovementFactor;
        }

        public virtual void StayStill()
        {
            //Do nothing!
            XMovementMagnitude = 0;
        }

        public void SetPosition(Vector2 position)
        {
            this.Animations.Position = position;
        }

        public Vector2 GetPosition()
        {
            return this.Animations.Position;
        }

        public virtual void Jump()
        {
                        
        }

        public void Crouch()
        {
            
        }

        public abstract void Attack();

        /// <summary>
        /// Updates the object's state
        /// </summary>
        /// <param name="gameTime"></param>
        public abstract void Update(int gameTime);

        /// <summary>
        /// Draws the object
        /// </summary>
        /// <param name="batch"></param>
        public void Draw(SpriteBatch batch)
        {
            if (DrawOnScreen)
                this.Animations.currentAnimation.Draw(batch, isFacingLeft);
        }

        protected abstract void AssembleAnimations();

        public  void SetAnimation(CharacterAnimationsList animation)
        {
            Animations.SetCurrentAnimation(animation.ToString());
        }

        public void SetActualState(IState state)
        {
            ActualState = state;
        }

        public bool IsCurrentAnimationDone()
        {
            return Animations.currentAnimation.Done;
        }
    }
}
