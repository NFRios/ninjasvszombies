﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Textures;
using GameMechanics.StateMachines;
using GameMechanics.StateMachines.Ninja;
using Handlers;
using Commands.Commands;
using Textures.Animations;
using System;
using Services;
using Services.Audio;

namespace GameMechanics.Characters.Heroes
{
    public abstract class Ninja:GameCharacter
    {
        public IState IdleState { get; set; }
        public IState AttackState { get; set; }
        public IState RunState { get; set; }
        public IState JumpState { get; set; }
        public IState AttackWhileJumpState { get; set; }

        public bool Attacking = false;

        protected IHandler handler;

        public Ninja(Vector2 position, TexturePack txPack, float animationScale)
        {           

            this.handler = new InputHandler();
            this.CharTextures = txPack;
            this.Scale = animationScale;
            AssembleAnimations();

            this.Position = position;
            MovementFactor = 8;
            IsOnGround = true;
        }

        public override void Update(int gameTime)
        {
            ICommand command = this.HandleInput();
            this.ActualState.Update(gameTime, command);
            this.Animations.currentAnimation.Update(gameTime);
        }

        public override void StayStill()
        {
            //this.ActualState = IdleState;
            base.StayStill();
        }

        public override void MoveLeft()
        {
            // this.ActualState = RunState;
            base.MoveLeft();
        }

        public override void MoveRight()
        {
            //this.ActualState = RunState;
            base.MoveRight();
        }

        public override void Jump()
        {
            //this.ActualState = JumpState;
            ServiceLocator.GetService<IAudioService>().PlayEffect("JumpSFX", 0.2f);
            base.Jump();
        }

        public ICommand HandleInput()
        {
            return handler.HandleInput();
        }

    }
}
