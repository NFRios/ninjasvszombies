﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Textures;
using GameMechanics.StateMachines;
using GameMechanics.StateMachines.Ninja;
using Handlers;
using Commands.Commands;
using Textures.Animations;
using Services;
using Services.Audio;

namespace GameMechanics.Characters.Heroes
{
    public class MaleNinja : Ninja, INinja
    {

      

        public MaleNinja(Vector2 position, TexturePack txPack, float animationScale):base(position,txPack, animationScale)
        {
            IdleState = new IdleState(this);
            AttackState = new AttackState(this);
            RunState = new MoveState(this);
            JumpState = new JumpState(this);
            AttackWhileJumpState = new AttackWhileJumpState(this);
            this.ActualState = IdleState;

        }

        public override void Attack()
        {
            ServiceLocator.GetService<IAudioService>().PlayEffect("BladeAttackSFX", 0.3f);          
        }

        protected override void AssembleAnimations()
        {
            int SpriteSheetNoRows = 10;
            int SpriteSheetNoCols = 8;



            Animations = new CharacterAnimations();
            Texture2D actionsTexture = CharTextures.GetTexture("NinjaActions");
            Animation IdleAnimation = new Animation(ref actionsTexture, Scale, Position, new Vector2(0, 0), new Vector2(0, 1), SpriteSheetNoRows, SpriteSheetNoCols, 80);
            Animations.AddAnimation(IdleAnimation, CharacterAnimationsList.Idle.ToString());

            Animation AttackAnimation = new Animation(ref actionsTexture, Scale, Position, new Vector2(0, 4), new Vector2(1, 5), SpriteSheetNoRows, SpriteSheetNoCols, 35);
            Animations.AddAnimation(AttackAnimation, CharacterAnimationsList.Attack.ToString());

            Animation RunAnimation = new Animation(ref actionsTexture, Scale, Position, new Vector2(0, 2), new Vector2(1, 3), SpriteSheetNoRows, SpriteSheetNoCols, 40);
            Animations.AddAnimation(RunAnimation, CharacterAnimationsList.Run.ToString());

            Animation jumpAnimation = new Animation(ref actionsTexture, Scale, Position, new Vector2(0, 6), new Vector2(1, 7), SpriteSheetNoRows, SpriteSheetNoCols, 50);
            jumpAnimation.RepeatAnimation = false;
            Animations.AddAnimation(jumpAnimation, CharacterAnimationsList.Jump.ToString());

            Animation attackjumpAnimation = new Animation(ref actionsTexture, Scale, Position, new Vector2(0, 8), new Vector2(1, 9), SpriteSheetNoRows, SpriteSheetNoCols, 35);
            jumpAnimation.RepeatAnimation = false;
            Animations.AddAnimation(attackjumpAnimation, CharacterAnimationsList.AttackWhileJump.ToString());

            Animations.SetCurrentAnimation(CharacterAnimationsList.Idle.ToString());
            Animations.Position = Position;

        }
       
    }
}
