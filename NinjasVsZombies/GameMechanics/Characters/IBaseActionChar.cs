﻿using GameMechanics.StateMachines;
using Commands.Actors;
using Microsoft.Xna.Framework;

namespace GameMechanics.Characters
{
    public interface IBaseActionChar
    {
        IState ActualState { get; set; }

        int XMovementMagnitude { get; set; }

        Vector2 Position { get; set; }

        void Attack();

        void StayStill();

        void MoveLeft();

        void MoveRight();

        void Jump();

        void SetActualState(IState state);

        void SetAnimation(CharacterAnimationsList animation);

        bool IsCurrentAnimationDone();
    }
}
