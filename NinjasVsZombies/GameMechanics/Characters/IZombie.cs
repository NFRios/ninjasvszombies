﻿using GameMechanics.StateMachines;
using Commands.Actors;
using Microsoft.Xna.Framework;

namespace GameMechanics.Characters
{

    /// <summary>
    /// This interface allows All zombies to have the same states as long as they implement it.
    /// </summary>
    public interface IZombie:IActor,IBaseActionChar
    {
        IState AttackState { get; set; }
        IState MoveState { get; set; }
        IState IdleState { get; set; }
        IState DeadState { get; set; }
    }
}
