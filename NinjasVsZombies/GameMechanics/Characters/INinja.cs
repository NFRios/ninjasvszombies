﻿using GameMechanics.StateMachines;
using Commands.Actors;
using Microsoft.Xna.Framework;


namespace GameMechanics.Characters
{
    /// <summary>
    /// This interface allows female and male ninja to have the same states as long as they implement it.
    /// </summary>
    public interface INinja:IActor, IBaseActionChar
    {
        IState IdleState { get; set; }
        IState AttackState { get; set; }
        IState RunState { get; set; }
        IState JumpState { get; set; }
        IState AttackWhileJumpState { get; set; }
        
    }
}
