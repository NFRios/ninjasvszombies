﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameMechanics.StateMachines;

namespace GameMechanics.Characters.Enemies
{
    public class FemaleZombie : GameCharacter, IZombie
    {
        public IState AttackState
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public IState DeadState
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public IState IdleState
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public IState MoveState
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public override void Attack()
        {
            throw new NotImplementedException();
        }

        public override void Update(int gameTime)
        {
            throw new NotImplementedException();
        }

        protected override void AssembleAnimations()
        {
            throw new NotImplementedException();
        }
    }
}
