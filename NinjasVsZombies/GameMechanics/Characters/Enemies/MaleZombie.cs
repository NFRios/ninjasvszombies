﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

using Textures;
using GameMechanics.StateMachines;

namespace GameMechanics.Characters.Enemies
{
    public class MaleZombie : GameCharacter, IZombie
    {

        public MaleZombie(Vector2 position, TexturePack txPack, float animationScale)
        {

        }

        public IState AttackState { get; set; }

        public IState DeadState { get; set; }

        public IState IdleState { get; set; }

        public IState MoveState { get; set; }

        public override void Attack()
        {
            throw new NotImplementedException();
        }

        public override void Update(int gameTime)
        {
            throw new NotImplementedException();
        }

        protected override void AssembleAnimations()
        {
            throw new NotImplementedException();
        }
    }
}
