﻿using Microsoft.Xna.Framework;

namespace GameMechanics
{
    public static class GameConstants
    {
        public const int NINJA_SWORD_DAMAGE = 10;
        public const int NINJA_KUNAI_DAMAGE = 5;

        public const int NINJA_HEALTH = 1000;
        public const int ZOMBIE_DAMAGE = 30;
        public const int ZOMBIE_HEALTH = 200;

        public static Vector2 GravityVector = new Vector2(0, 1f);

        public static float FloorYPosition = 555f;
    }
}
