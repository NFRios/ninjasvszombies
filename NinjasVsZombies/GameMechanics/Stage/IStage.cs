﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GameMechanics.Stage
{
    public interface IStage
    {
        void Update(int gameTime);

        void Draw(SpriteBatch batch);

      
    }
}
