﻿
using Textures.BackGrounds;
using GameMechanics.Characters.Heroes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using Textures;
using Services;
using Services.Audio;
using Microsoft.Xna.Framework.Audio;
using GameMechanics.Characters;

namespace GameMechanics.Stage
{
    public class Stage:IStage
    {
        private BackGround stageBackGround;
        private Vector2 worldSize;
        private Ninja player;
       
        private TexturePack texturePack;
        private SoundLibrary soundLibrary;

        private int lowerLimit= 200;
        private int upperLimit = 1000;

        public Stage(Vector2 worldSize)
        {           
            this.worldSize = worldSize;
            texturePack = new TexturePack();
        }

        /// <summary>
        /// Draws all Stage Content
        /// </summary>
        /// <param name="batch"></param>
        public void Draw(SpriteBatch batch)
        {
            stageBackGround.Draw(batch);
            player.Draw(batch);
        }

        /// <summary>
        /// Updates all stage Content
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(int gameTime)
        {
            if (player.GetPosition().X < lowerLimit)
            {
                stageBackGround.Position = new Vector2(stageBackGround.Position.X + player.MovementFactor, stageBackGround.Position.Y);
                player.SetPosition(new Vector2(lowerLimit, player.GetPosition().Y));
            }
            if (player.GetPosition().X > upperLimit )
            {
                stageBackGround.Position = new Vector2(stageBackGround.Position.X - player.MovementFactor, stageBackGround.Position.Y);
                player.SetPosition(new Vector2(upperLimit, player.GetPosition().Y));
            }
            player.Update(gameTime);
            stageBackGround.Update(gameTime);
        }

        public void LoadContent(ContentManager content)
        {
            loadPlayer(content, PlayableChars.FemaleNinja);
            loadBackGround(content);
            loadSound(content);
        }

        private void loadBackGround(ContentManager content)
        {
            stageBackGround = new BackGround(new Rectangle(0, 0, (int)worldSize.X, (int)worldSize.Y));
            stageBackGround.AddLayer(content.Load<Texture2D>(@"Graphics/BackGround/Forest/Forest_07"), new Vector2(0, 0));
            stageBackGround.AddLayer(content.Load<Texture2D>(@"Graphics/BackGround/Forest/Forest_06"), new Vector2(0, 0));
            stageBackGround.AddLayer(content.Load<Texture2D>(@"Graphics/BackGround/Forest/Forest_05"), new Vector2(0.5f, 0));
            stageBackGround.AddLayer(content.Load<Texture2D>(@"Graphics/BackGround/Forest/Forest_04"), new Vector2(0.8f, 0));
            stageBackGround.AddLayer(content.Load<Texture2D>(@"Graphics/BackGround/Forest/Forest_03"), new Vector2(0.8f, 0));
            stageBackGround.AddLayer(content.Load<Texture2D>(@"Graphics/BackGround/Forest/Forest_02"), new Vector2(1f, 0));
            stageBackGround.AddLayer(content.Load<Texture2D>(@"Graphics/BackGround/Forest/Forest_01"), new Vector2(1f, 0));
        }

        private void loadSound(ContentManager content)
        {
            soundLibrary = new SoundLibrary();
            soundLibrary.AddSound(content.Load<SoundEffect>(@"Sounds/BackGround/Music/TheBiggestDreamer"));
            soundLibrary.AddEffect(content.Load<SoundEffect>(@"Sounds/Ninja/BladeAttackSFX"));
            soundLibrary.AddEffect(content.Load<SoundEffect>(@"Sounds/Ninja/JumpSFX"));
            soundLibrary.AddEffect(content.Load<SoundEffect>(@"Sounds/Ninja/FemaleAttack"));
            ServiceLocator.GetService<IAudioService>().LoadSoundLibrary(soundLibrary);
            ServiceLocator.GetService<IAudioService>().PlaySound("TheBiggestDreamer", 0.1f, true);
        }
        
        private void loadPlayer(ContentManager content, PlayableChars chara)
        {
            switch(chara)
            {
                case PlayableChars.MaleNinja:
                    texturePack.AddTexture(content.Load<Texture2D>(@"Graphics/Chars/Ninja/NinjaActions"));
                    player = new MaleNinja(new Vector2(100, GameConstants.FloorYPosition), texturePack.GetTextures("NinjaActions"), 0.35f);
                    break;
                case PlayableChars.FemaleNinja:
                    texturePack.AddTexture(content.Load<Texture2D>(@"Graphics/Chars/Ninja/FemaleNinjaActions"));
                    player = new FemaleNinja(new Vector2(100, GameConstants.FloorYPosition), texturePack.GetTextures("FemaleNinjaActions"), 0.35f);
                    break;
            }
        }
    }
}
