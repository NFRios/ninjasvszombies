﻿using System;
using Commands.Actors;

namespace Commands.Commands
{
    /// <summary>
    /// A command that makes a game character move up, or look up
    /// </summary>
    public class UpCommand:ICommand
    {
        public CommandType Type
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Execute(IActor gameCharacter)
        {
            gameCharacter.MoveUp();
        }
    }
}
