﻿using Commands.Actors;

namespace Commands.Commands
{
    /// <summary>
    /// Command to make a gameCharacter Attack, implements the ICommand interface
    /// </summary>
    public class AttackCommand:ICommand
    {
        public CommandType Type { get { return CommandType.AttackCommand; } }
        public void Execute(IActor gameCharacter)
        {
            gameCharacter.Attack();
        }
    }
}
