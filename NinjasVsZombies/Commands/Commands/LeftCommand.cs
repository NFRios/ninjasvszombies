﻿using System;
using Commands.Actors;

namespace Commands.Commands
{
    /// <summary>
    /// A command to make a game character move left, implements the ICommand interface
    /// </summary>
    public class LeftCommand:ICommand
    {
        public CommandType Type
        {
            get
            {
                return CommandType.LeftCommand;
            }
        }

        public void Execute(IActor gameCharacter)
        {
            gameCharacter.MoveLeft();
        }
    }
}
