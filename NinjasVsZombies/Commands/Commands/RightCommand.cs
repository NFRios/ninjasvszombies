﻿using System;
using Commands.Actors;

namespace Commands.Commands
{
    /// <summary>
    /// Command that makes a game character move right, implements the ICommand interface
    /// </summary>
    public class RightCommand:ICommand
    {
        public CommandType Type
        {
            get
            {
                return CommandType.RightCommand;
            }
        }

        public void Execute(IActor actor)
        {
            actor.MoveRight();
        }
    }
}
