﻿using System;
using Commands.Actors;

namespace Commands.Commands
{
    /// <summary>
    /// A command to move down a game character, implement the ICommand Interface
    /// </summary>
    public class DownCommand:ICommand
    {
        public CommandType Type
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Execute(IActor gameCharacter)
        {
            gameCharacter.MoveDown();
        }
    }
}
