﻿using System;
using Commands.Actors;

namespace Commands.Commands
{
    /// <summary>
    /// A command to make a game character jump, implements the ICommand interface
    /// </summary>
    public class JumpCommand:ICommand
    {
        public CommandType Type
        {
            get
            {
                return CommandType.JumpCommand;
            }
        }

        public void Execute(IActor gameCharacter)
        {
            gameCharacter.Jump();
        }
    }
}
