﻿
using System;
using Commands.Actors;

namespace Commands.Commands
{
    /// <summary>
    /// A command to make the game character move to a certain screen coordinate, implements the ICommandInterface
    /// </summary>
    public class MoveToCommand:ICommand
    {
        public CommandType Type
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public int X { get; set; }
        public int Y { get; set; }

        public void Execute(IActor gameCharacter)
        {
            gameCharacter.MoveTo(X, Y);
        }

    }
}
