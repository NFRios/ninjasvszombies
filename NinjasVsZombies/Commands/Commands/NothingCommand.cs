﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Commands.Actors;
using Commands;

namespace Commands.Commands
{
    public class NothingCommand : ICommand
    {
        public CommandType Type
        {
            get
            {
                return CommandType.NothingCommand;
            }
        }

        public void Execute(IActor actor)
        {
            actor.StayStill();
        }
    }
}
