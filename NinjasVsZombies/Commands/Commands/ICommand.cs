﻿
using Commands.Actors;

namespace Commands.Commands
{
    /// <summary>
    /// Define a common interface for commands to be executed
    /// </summary>
    public interface ICommand
    {
        CommandType Type { get;}
        void Execute(IActor actor);
    }
}
