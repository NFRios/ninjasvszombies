﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace GameMechanics
{
    ///<summary>
    /// Interface that defines methods and properties that position any object that implements this interface
    /// </summary>
    public interface IPositionable
    {
        Vector2 Position { get; set; }
        bool IsOnGround { get; set; }

        void SetPosition(Vector2 position);

        Vector2 GetPosition();
    }
}
