﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commands.Actors
{
    /// <summary>
    /// The object that will be commanded
    /// </summary>
    public interface IActor
    {  

        /// <summary>
        /// Moves the actor to a certain location
        /// </summary>
        /// <param name="x">X location</param>
        /// <param name="y">Y location</param>
        void MoveTo(int x, int y);

        /// <summary>
        /// Move the actor up
        /// </summary>
        void MoveUp();

        /// <summary>
        /// Move the actor down
        /// </summary>
        void MoveDown();

        /// <summary>
        /// Move the actor left
        /// </summary>
        void MoveLeft();

        /// <summary>
        /// Move the actor right
        /// </summary>
        void MoveRight();

        /// <summary>
        /// Makes the actor jump
        /// </summary>
        void Jump();

        /// <summary>
        /// Makes the actor crouch
        /// </summary>
        void Crouch();

        /// <summary>
        /// Makes the actor attack
        /// </summary>
        void Attack();

        /// <summary>
        /// Makes the actor stay still
        /// </summary>
        void StayStill();
    }
}
