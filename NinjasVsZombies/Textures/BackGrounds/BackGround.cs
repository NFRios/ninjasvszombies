﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Textures.BackGrounds
{
    public class BackGround
    {

        public Vector2 Position;

        private List<BackGroundLayer> layerList = new List<BackGroundLayer>();
        private Rectangle layerRectangle;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="LayerRectangle">Rectangle over which the background will be drawn</param>
        public BackGround(Rectangle LayerRectangle)
        {
            layerRectangle = LayerRectangle;
        }

        public void AddLayer(Texture2D texture, Vector2 layerSpeedFactor, int index)
        {
            if (index <= layerList.Count)
                layerList.Insert(index, new BackGroundLayer(texture, layerSpeedFactor, layerRectangle));
        }

        public void AddLayer(Texture2D texture, Vector2 speed)
        {
            layerList.Add(new BackGroundLayer(texture, speed, layerRectangle));
        }

        public void Update(int gameTime)
        {
            foreach(var layer in layerList)
            {
                layer.Position = this.Position;
                layer.Update(gameTime);                
            }
        }

        public void Draw(SpriteBatch batch)
        {
            foreach(var layer in layerList)
            {
                layer.Draw(batch);
            }
        }
    }
}
