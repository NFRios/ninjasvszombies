﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;



namespace Textures.BackGrounds
{
    internal class BackGroundLayer
    {      
        public Vector2 Position;
     
        private Texture2D layer { get; set; }
        private Rectangle layerRectangle;
        private Rectangle layerRectangle2;
        private Vector2 layerFactor;

        public BackGroundLayer(Texture2D texture, Vector2 layerFactor, Rectangle layerRectangle)
        {
            layer = texture;
            this.layerFactor = layerFactor;
            this.layerRectangle = layerRectangle;
            layerRectangle2 = layerRectangle;   
        }

        public void Update(int gameTime)
        {
            Vector2 layerPosition = Position * layerFactor;

            layerRectangle.X = (int)((layerPosition.X % layerRectangle.Width));

    
            layerRectangle2.X = (int)layerRectangle.X - layerRectangle.Width;

            if (layerRectangle.Location.X < 0)
                layerRectangle2.Location = new Point(layerRectangle.Location.X + layerRectangle.Size.X, layerRectangle2.Location.Y);

        }

        public void Draw(SpriteBatch batch)
        {
            //Dibuja la capa en su posicion inicial
            batch.Draw(layer, layerRectangle, null, Color.White);
            //Dibuja la capa en donde termina la anterior, eso para crear un efecto infinito
            batch.Draw(layer, layerRectangle2, null, Color.White);
        }



    }
}
