﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Textures.Animations
{
    public class CharacterAnimations
    {

        public Vector2 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
                foreach (Animation anim in animations.Values)
                {
                    anim.AnimationPosition = position;
                }
            }
        }

        private Vector2 position;

        private Dictionary<string, Animation> animations = new Dictionary<string, Animation>();
        public Animation currentAnimation { get; private set; }


        public CharacterAnimations()
        {

        }

        public void SetCurrentAnimation(string animationName)
        {
            Animation anim = getAnimation(animationName);

            if (currentAnimation != anim)
            {
                if (currentAnimation != null)
                {
                    currentAnimation.Done = false;
                    currentAnimation.Reset();
                }

                currentAnimation = anim;
            }
        }


        /// <summary>
        /// Returns an animation by its name, it throws an exception if such animation doesn't exist
        /// </summary>
        /// <param name="name">Animation's name</param>
        /// <returns>Animation</returns>
        public Animation this[string name]
        {
            get
            {
                return getAnimation(name);
            }
        }

        /// <summary>
        /// Adds an animation to the internal collection of animations
        /// </summary>
        /// <param name="animation">Animation to add</param>
        /// <param name="name">Animation's name</param>
        public void AddAnimation(Animation animation, string name)
        {
            if (!animations.ContainsKey(name))
            {
                animation.AnimationPosition = this.position;
                animations.Add(name, animation);
            }
        }

        /// <summary>
        /// Gets an animation by its name, it throws an exception if such animation doesn't exist
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Animation getAnimation(string name)
        {
            if (animations.ContainsKey(name))
            {
                return animations[name];
            }
            else
            {
                throw new Exception("There's not such animation in the animation manager");
            }
        }        
    }
}
