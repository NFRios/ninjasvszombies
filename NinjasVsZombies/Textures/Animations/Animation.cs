﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Textures.Animations
{
    public class Animation : IAnimation
    {

        public bool Done { get; set; }
        public Vector2 AnimationPosition
        {
            get
            {
                return animationPosition;
            }

            set
            {
                    animationPosition = new Vector2(value.X, value.Y - this.FrameSize.Y);
            }
        }

        private  Vector2 animationPosition;

        public int actualFrame;
        public Vector2 FrameSize
        {
            get {
                return new Vector2(sourceRectangle.Width * animationScale, sourceRectangle.Height * animationScale);
            }
        }

        public bool RepeatAnimation
        {
            get;set;
        }

        private float animationScale;
        private Vector2 initialFramePosition;
        private Vector2 finalFramePosition;
        private int numberOfRows;
        private int numberOfColumns;
        private Vector2 spriteSheetSize;
        private int timePerFrame;
        private Texture2D spriteSheet;


        private Rectangle sourceRectangle;
        private int totalTime = 0;
        private int initialFrame;
        private int finalFrame;
        private int factor = 1;

        public Animation(ref Texture2D spriteSheet, float animationScale, Vector2 animationPosition, Vector2 initialFramePosition, Vector2 finalFramePosition,
            int numberOfRows, int numberOfColumns, int timePerFrame)
        {
            this.animationPosition = animationPosition;
            this.spriteSheet = spriteSheet;
            this.animationScale = animationScale;
            this.initialFramePosition = initialFramePosition;
            this.finalFramePosition = finalFramePosition;
            this.numberOfRows = numberOfRows;
            this.numberOfColumns = numberOfColumns;
            this.timePerFrame = timePerFrame;
            parametrizeAnimation();

            MoveSourceRectangle(actualFrame);
            RepeatAnimation = true;

        }

        private void parametrizeAnimation()
        {
            spriteSheetSize = new Vector2(spriteSheet.Width, spriteSheet.Height);
            int frameSizeX = (int)spriteSheetSize.X / numberOfColumns;
            int frameSizeY = (int)spriteSheetSize.Y / numberOfRows;

            sourceRectangle = new Rectangle(0, 0, frameSizeX, frameSizeY);
            initialFrame = CartesianToLineal(initialFramePosition);
            finalFrame = CartesianToLineal(finalFramePosition);
            actualFrame = initialFrame;
            Done = false;

            if (initialFrame > finalFrame)
                factor = -1;

            if (initialFramePosition.X > numberOfColumns)
                initialFramePosition.X = numberOfColumns;

            if (initialFramePosition.X < 0)
                initialFramePosition.X = 0;

            if (initialFramePosition.Y > numberOfRows)
                initialFramePosition.Y = numberOfRows;

            if (initialFramePosition.Y < 0)
                initialFramePosition.X = 0;
        }

        public void Update(int miliseconds)
        {
            totalTime += miliseconds;

            Done = false;

            if (totalTime >= timePerFrame)
            {
                totalTime = 0;

                if (actualFrame > finalFrame)
                {
                    Done = true;

                    if (RepeatAnimation)
                        actualFrame = initialFrame;
                    else
                        actualFrame = finalFrame;                                              
                }
                else
                { Done = false; }
                MoveSourceRectangle(actualFrame);
                actualFrame = actualFrame + factor;

            }
        }

        public void Draw(SpriteBatch batch, bool flipLeft)
        {

            Vector2 defPosition = new Vector2(AnimationPosition.X - (sourceRectangle.Width * animationScale)/2, AnimationPosition.Y);
               
            batch.Draw(spriteSheet, flipLeft? defPosition: AnimationPosition, null, sourceRectangle, null, 0, new Vector2(1 * animationScale, 1 * animationScale), Color.White, flipLeft ? SpriteEffects.FlipHorizontally : SpriteEffects.None, 0);
        }

        private int CartesianToLineal(Vector2 coordenate)
        {
            return (int)coordenate.Y * numberOfColumns + (int)coordenate.X;
        }

        private Vector2 LinealToCartesian(int position)
        {
            float x = position % numberOfColumns;
            float y = position / numberOfColumns;

            return new Vector2(x, y);
        }

        /// <summary>
        /// Resets the animation to its original values
        /// </summary>
        public void Reset()
        {
            actualFrame = initialFrame;
            MoveSourceRectangle(actualFrame);

        }

        /// <summary>
        /// Moves the source rectangle on the spriteSheet in order to show a frame
        /// </summary>
        /// <param name="actualFrame">the frame to be moved onto</param>
        private void MoveSourceRectangle(int actualFrame)
        {
            Vector2 actualPosition = LinealToCartesian(actualFrame);
            sourceRectangle.X = sourceRectangle.Width * (int)actualPosition.X;
            sourceRectangle.Y = sourceRectangle.Height * (int)actualPosition.Y;
        }
    }
}
