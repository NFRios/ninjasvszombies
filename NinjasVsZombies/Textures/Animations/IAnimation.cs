﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Textures.Animations
{
    public interface IAnimation
    {       

        bool Done { get; set; }

        void Update(int miliseconds);

        void Draw(SpriteBatch spriteBatch, bool flipHorizontally);

        void Reset();
    }
}
