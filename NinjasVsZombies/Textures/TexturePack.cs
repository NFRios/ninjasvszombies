﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework.Graphics;



namespace Textures
{
    public class TexturePack
    {
        private Dictionary<string, Texture2D> textures = new Dictionary<string, Texture2D>();

        /// <summary>
        /// Returns a texture which name contains the key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public Texture2D this[string key]
        {
            get
            {
               return GetTexture(key);
            }
        }

        public void AddTexture(Texture2D texture)
        {
            if (!textures.ContainsKey(texture.Name))
            {
                textures.Add(texture.Name, texture);
            }
            else
            {
                textures[texture.Name] = texture;
            }
        }

        public void AddTextures(IDictionary<string, Texture2D> textures)
        {
            foreach(var texture in textures)
            {
                if(!textures.ContainsKey(texture.Key))
                {
                    this.textures.Add(texture.Key, texture.Value);
                }
                else
                {
                    this.textures[texture.Key] = texture.Value;
                }
            }
        }        

        public Texture2D GetTexture(string name)
        {
            Texture2D ret = null;

            foreach (var pair in textures)
            {
                if (pair.Key.ToLower().Contains(name.ToLower()))
                {
                    ret = pair.Value;
                    break;
                }
            }
            return ret;
        }

        public TexturePack GetTextures(string filterName)
        {
            var filtered = textures.Where(s => s.Key.ToLower().Contains(filterName.ToLower())).ToDictionary(x => x.Key, x => x.Value);
            var ret = new TexturePack();

            ret.AddTextures(filtered);
            return ret;
        }
    }
}
