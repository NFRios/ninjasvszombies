﻿using Commands.Commands;
using Commands.Actors;
using Microsoft.Xna.Framework.Input;

namespace Handlers
{
    public class InputHandler: HandlerBase
    {

        KeyBoardManager Kbm = new KeyBoardManager();
        public InputHandler()
        {
            upButtonCommand = new UpCommand();
            downButtonCommand = new DownCommand();
            leftButtonCommand = new LeftCommand();
            rightButtonCommand = new RightCommand();
            attackCommand = new AttackCommand();
            nothingCommand = new NothingCommand();
            jumpCommand = new JumpCommand();
            
        }

        /// <summary>
        /// Call this in an update method!
        /// </summary>
        public override ICommand HandleInput()
        {

            Kbm.GetActualState();

            if (Kbm.IsKeyDown(Keys.A))
                return attackCommand;
            if (Kbm.WasKeyPressed(Keys.S))
                return jumpCommand;
            if (Kbm.IsKeyDown(Keys.Left))
                return leftButtonCommand;
            if (Kbm.IsKeyDown(Keys.Right))
                return rightButtonCommand;
            if (Kbm.IsKeyDown(Keys.Up))
                return upButtonCommand;
            if (Kbm.IsKeyDown(Keys.Down))
                return downButtonCommand;

            return nothingCommand;
        }       
    }
}
