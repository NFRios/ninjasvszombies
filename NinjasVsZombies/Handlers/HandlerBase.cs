﻿using Commands.Actors;
using Commands.Commands;

namespace Handlers
{
    public abstract class HandlerBase: IHandler
    {
        protected IActor gameCharacter;

        protected ICommand upButtonCommand;
        protected ICommand downButtonCommand;
        protected ICommand leftButtonCommand;
        protected ICommand rightButtonCommand;
        protected ICommand nothingCommand;
        protected ICommand jumpCommand;
        protected ICommand attackCommand;
        public abstract ICommand HandleInput();
    }
}
