﻿using Microsoft.Xna.Framework.Input;

namespace Handlers
{
    public class KeyBoardManager
    {
        KeyboardState oldState;
        KeyboardState newState;


        public void GetActualState()
        {
            oldState = newState;
            newState = Keyboard.GetState();
        }

        public bool WasKeyPressed(Keys key)
        {
            bool up = oldState.IsKeyUp(key);
            bool down = newState.IsKeyDown(key);
            return (down && up);

        }

        public bool IsKeyDown(Keys key)
        {
            return newState.IsKeyDown(key);
        }


        public bool WasKeyReleased(Keys key)
        {
            bool up = newState.IsKeyUp(key);
            bool down = oldState.IsKeyDown(key);
            return up && down;
        }
    }
}
