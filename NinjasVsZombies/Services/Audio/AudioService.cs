﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;
namespace Services.Audio
{
    public class AudioService : IAudioService
    {
        ISoundLibrary soundLibrary;

        public AudioService(ISoundLibrary soundLibrary)
        {
            this.soundLibrary = soundLibrary;
        }

        public AudioService()
        {

        }


        public void LoadSoundLibrary(ISoundLibrary soundLibrary)
        {            
            this.soundLibrary = soundLibrary;
        }

        public void PlaySound(string sound, float volume, bool isLooped)
        {
           if (soundLibrary != null && soundLibrary.NumberOfSounds > 0)
            {
                SoundEffectInstance s = soundLibrary.GetSound(sound);                
                s.Volume = volume;
                s.IsLooped = isLooped;
                if (s.State == SoundState.Playing)
                    s.Stop();
                s.Play();
            }
        }        

        public void StopAllSound()
        {
            for (int i = 0; i < soundLibrary.NumberOfSounds; i++)
            {
                soundLibrary.GetSound(i).Stop();
            }

        }

        public void StopSound(string sound)
        {
            soundLibrary.GetSound(sound).Stop();
        }

        public void UnloadSoundLibrary()
        {
            StopAllSound();
            throw new NotImplementedException();
        }

        public void PlayEffect(string sound, float volume)
        {
            SoundEffect s = soundLibrary.GetSoundEffect(sound);
            s.Play(volume, 0, 0);
        }
    }
}
