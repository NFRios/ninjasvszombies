﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework.Audio;


namespace Services.Audio
{
    public class SoundLibrary : ISoundLibrary
    {
        private Dictionary<string, SoundEffectInstance> library;
        private Dictionary<string, SoundEffect> effectsLibrary;

        public int NumberOfSounds
        {
            get
            {
                return library.Count;
            }
        }

        public SoundLibrary()
        {
            library = new Dictionary<string, SoundEffectInstance>();
            effectsLibrary = new Dictionary<string, SoundEffect>();
        }

        public void AddSound(SoundEffect sound)
        {
            string soundName = TrimAudioName(sound.Name);

            if (library.ContainsKey(soundName))
            {
                library[soundName] = sound.CreateInstance();
            }
            else
            {
                library.Add(soundName, sound.CreateInstance());
            }
        }

        public void AddEffect(SoundEffect sound)
        {
            string soundName = TrimAudioName(sound.Name);

            if (effectsLibrary.ContainsKey(sound.Name))
            {
                effectsLibrary[soundName] = sound;
            }
            else
            {
                effectsLibrary.Add(soundName, sound);
            }
        }

        public SoundEffectInstance GetSound(string name)
        {

            if (library.ContainsKey(name))
            {
                return library[name];
            }
            else
            {
                throw new Exception("There is not such sound in the library");
            }
        }

        public SoundEffect GetSoundEffect(string name)
        {
            if (effectsLibrary.ContainsKey(name))
            {
                return effectsLibrary[name];
            }
            else
            {
                throw new Exception("There is not such effect in the library");
            }
        }

        public SoundEffectInstance GetSound(int index)
        {
            List<SoundEffectInstance> l = library.Values.ToList<SoundEffectInstance>();
            return l[index];
        }

        public void EraseLibrary()
        {
            foreach (string name in library.Keys)
            {
                library[name].Dispose();
                library[name] = null;
                library.Remove(name);
            }

        }

        private string TrimAudioName(string audioName)
        {
            int index = 0;
            for (int i = 0; i < audioName.Length; i++)
            {
                if (audioName[i] == '/')
                {
                    index = i;
                }
            }

            return audioName.Substring(index + 1);
        }
    }
}
