﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework.Audio;

namespace Services.Audio
{
    public interface IAudioService
    {
        /// <summary>
        /// Plays a sound
        /// </summary>
        /// <param name="sound">The sound's name</param>
        /// <param name="volume">The sound's volume, max 1f</param>
        /// <param name="isLooped">Is the sound gonna play continously</param>
        void PlaySound(string sound, float volume, bool isLooped);

        void PlayEffect(string sound, float volume);

        /// <summary>
        /// Stops a given sound
        /// </summary>
        /// <param name="sound"></param>
        void StopSound(string sound);

        /// <summary>
        /// Stops all sound playing
        /// </summary>
        void StopAllSound();

        /// <summary>
        /// Loads a collection of sounds into the service
        /// </summary>
        void LoadSoundLibrary(ISoundLibrary soundLibrary);

        /// <summary>
        /// Unload the current sound collection
        /// </summary>
        void UnloadSoundLibrary();
    }
}
