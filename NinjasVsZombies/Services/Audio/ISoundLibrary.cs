﻿using Microsoft.Xna.Framework.Audio;

namespace Services.Audio
{
    public interface ISoundLibrary
    {
        int NumberOfSounds { get; }
        void AddSound(SoundEffect sound);
        void AddEffect(SoundEffect sound);
        void EraseLibrary();
        SoundEffectInstance GetSound(string name);
        SoundEffectInstance GetSound(int index);
        SoundEffect GetSoundEffect(string name);


    }
}