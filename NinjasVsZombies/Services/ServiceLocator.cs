﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services.Audio;

namespace Services
{
    public static class ServiceLocator
    {
        /// <summary>
        /// Singleton
        /// </summary>
        private static IAudioService service;

        public static T GetService<T>() where T:class
        {
            if (typeof(T).Equals(typeof(IAudioService)))
            {
                if (service == null)
                {
                    service = new AudioService();
                    return service as T; 
                }
                else
                {
                    return service as T;
                }
            }

            return null; 
        }

    }
}
